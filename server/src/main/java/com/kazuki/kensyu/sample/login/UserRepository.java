
package com.kazuki.kensyu.sample.login;

// import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
// import org.springframework.stereotype.Repository;

/**
 * UserRepository
 */
// @Repository
@RepositoryRestResource
public interface UserRepository extends JpaRepository<User, Long> {

    public User findByEmailAddress(String emailAddress);
    public User findByUserName(String userName);

}