package com.kazuki.kensyu.sample.login;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 *  DTO Class For User Info
 *  @author Kazuki
 */

@Entity
@Table(name = "users")
public class User{

    @Id @GeneratedValue(strategy = GenerationType.IDENTITY) @Column(name = "Id")
    private Long Id;

    @Column(name = "username")
    private String userName;

    @Column(name = "email")
    private String emailAddress;

    @Column(name = "password")
    private String passWord;

    public User(){}

    public User(String userName, String emailAddress, String passWord) {
        this.userName = userName;
        this.emailAddress = emailAddress;
        this.passWord = passWord;
    }

    /**
     * @param id the id to set
     */
    public void setId(Long id) {
        Id = id;
    }

    /**
     * @return the id
     */
    public Long getId() {
        return Id;
    }

    /**
     * @param userName the userName to set
     */
    public void setUserName(String userName) {
        this.userName = userName;
    }

    /**
     * @return the userName
     */
    public String getUserName() {
        return userName;
    }

    /**
     * @param emailAddress the emailAddress to set
     */
    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }

    /**
     * @return the emailAddress
     */
    public String getEmailAddress() {
        return emailAddress;
    }

    /**
     * @param passWord the passWord to set
     */
    public void setPassWord(String passWord) {
        this.passWord = passWord;
    }

    /**
     * @return the passWord
     */
    public String getPassWord() {
        return passWord;
    }

}