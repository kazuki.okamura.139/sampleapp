package com.kazuki.kensyu.sample.login;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.transaction.Transactional;

import com.fasterxml.jackson.databind.util.JSONPObject;

import org.springframework.web.bind.annotation.CrossOrigin;
// import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * UserController
 */
@CrossOrigin(origins = "http://localhost:8081")
@RestController
public class UserController {

    private UserRepository repository;
    public String data;
    public Optional<User> loggedinuser;
    public String result;


    public UserController(UserRepository repository) {
        this.repository = repository;
    }

    @RequestMapping("/login")
    public Collection<User> showUser() {
        // test stream
        // this is not related to database
        return this.repository.findAll().stream().collect(Collectors.toList());
    }

    /**
     * @param newuser
     * @return
     * Register User to DB
     */
    @RequestMapping(path = "/registeruser", method = RequestMethod.POST)
    public List<String> registerUser(@RequestBody User newuser){
        //data passed from react 
        // @todo send this to mysql and save

        List<String> result = new ArrayList<>();

        if( (this.repository.findByEmailAddress(newuser.getEmailAddress()) == null) && (this.repository.findByUserName(newuser.getUserName()) == null) ){
            this.repository.save(newuser);

            result.add(newuser.getUserName());
            result.add("true");
            return result;

        }else if(this.repository.findByEmailAddress(newuser.getEmailAddress()) != null){
            result.add(newuser.getEmailAddress());
            result.add("false");
            return result;
        }else if(this.repository.findByUserName(newuser.getUserName()) != null){
            result.add(newuser.getPassWord());
            result.add("false");
            return result;
        }else{
            result.add("Error occured");
            result.add("false");
            return result;
        }
    }

    /**
     * Get Registered User from DB
     * @return
     */
    @RequestMapping(path = "/registeruser", method = RequestMethod.GET)
    public String registerUser(){
        return data;
    }

    /**
     * Login Controller to check if data matchs
     * @param loginuser
     * @return
     */
    @RequestMapping(path = "/loginuser", method = RequestMethod.POST)
    public Boolean loginUser(@RequestBody User loginuser){

        
        result = this.repository.findByEmailAddress(loginuser.getEmailAddress()).getPassWord().trim();

        if(result.equals(loginuser.getPassWord())){
            return true;
        }
        return false;
    }

    /**
     * Get loggedin user data
     * @return
     */
    @RequestMapping(path = "/postuser", method = RequestMethod.GET)
    public String loginUser(String data){
        return data;
    }

    @RequestMapping(path = "/postuser", method = RequestMethod.GET)
    public String postUser(@RequestBody String data1){
        // just to show data for testing
        data = data1;
        return data;
    }

    @RequestMapping(path = "/allusers", method = RequestMethod.GET)
    @Transactional
    public List<User> getAll(){
        //listing all users from database
        return this.repository.findAll();
    }

    // @RequestMapping("/test")
    // public String testShow(){
    //     return "test Can you see this?";
    // }
    
}