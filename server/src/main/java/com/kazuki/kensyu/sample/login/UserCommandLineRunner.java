
// package com.kazuki.kensyu.sample.login;

// import org.springframework.beans.factory.annotation.Autowired;
// import org.springframework.boot.CommandLineRunner;
// import org.springframework.stereotype.Component;

/**
 * UserCommandLineRunner
 */
// @Component
// public class UserCommandLineRunner implements CommandLineRunner{

//     private final UserRepository repository;


//     @Autowired
//     public UserCommandLineRunner(UserRepository repository) {
//         this.repository = repository;
//     }

//     @Override
//     public void run(String... args) throws Exception {
//         // this.repository.save(new User("sample", "sample@email.com", "password123"));

//         repository.findAll().forEach(System.out::println);
//     }
    
// }