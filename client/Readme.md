# React　(View側)

### アクセス制御

- src/_helper配下に簡単な制御を実装  
authentication.js にてログインの有無をチェック
```jsx
    Authentication.authenticate() // ログイン状態をTrueに    
    Authentication.signout() // ログイン状態をFalseに    
```

- PrivateRouterを作成し、アクセスを制限するコンポーネントを指定する。
```jsx
// myAuth.js

// 引数のcomponent: Component は渡されてきた制御したいコンポーネント。
// ...restは他の渡されてきたprops(詳細は省略)
// PrivateRouteが呼ばれると<Route> を返す。
// render={}で返すコンポーネントをログインの有無で変更しているだけ
// trueの場合：　受け取ったコンポーネントをそのまま返す
// falseの場合: ログインにRedirect.
const PrivateRoute = ({ component: Component, ...rest }) => (
    <Route {...rest} render={(props) => (
        Authentication.isAuthenticated
          ? <Component {...props} />
          : <Redirect to={{
                pathname: '/login',
                state: { from: props.location }
                }} 
            />
        )} 
    />
)
```

- 呼び出しているとこ
```jsx
// LandingPage.js
    <Switch>
        <Route exact path="/" component={WelocomePage} />　// 誰でも
        <Route exact path="/register" component={RegisterForm} />　// 誰でも
        <Route exact path="/login" component={LoginForm} />　// 誰でも
        <PrivateRoute path="/dashboard" component={DashBoard}/> // 制御
    </Switch>
```