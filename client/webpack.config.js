module.exports = {
    mode: "development",
    // devServer:{
    //     contentBase: __dirname + '/public/dist',
    //     historyApiFallBack: true,
    // },
    entry: "./src/index.js",
    output: {
        path: __dirname + '/public/dist',
        filename: 'bundle.js'
    },
    module: {
        rules: [
            {
                test: /\.(js|jsx)?$/,
                exclude: /node_modules/,
                use: ['babel-loader']
            },
        ]
    },
    resolve: {
        extensions: ['.js', '.jsx']
    }
};