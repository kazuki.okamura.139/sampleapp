import React from 'react';
import ReactDOM from 'react-dom';

import { Provider } from 'react-redux';
import { Router, BrowserRouter, Route } from 'react-router-dom';

import App from './components/App'
import store from './_helper/store'
// import history from './_helper/history'

ReactDOM.render(
    <BrowserRouter>
        <Provider store={store}>
                <App />
        </Provider>
    </BrowserRouter>,
    document.getElementById('root')
);