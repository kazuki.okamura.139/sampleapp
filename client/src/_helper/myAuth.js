import React, { Component } from "react";
import { Authentication } from "./authentication";
import { Route, Redirect } from "react-router-dom";

export const PrivateRoute = ({ component: Component, ...rest }) => (
    <Route {...rest} render={(props) => (
        Authentication.isAuthenticated
          ? <Component {...props} />
          : <Redirect to={{
                pathname: '/login',
                state: { from: props.location }
                }} 
            />
        )} 
    />
)