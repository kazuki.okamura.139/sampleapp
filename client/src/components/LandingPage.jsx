import React, { Component } from "react";

import PropTypes from 'prop-types';

import { Grid, withStyles } from "@material-ui/core";
import { Link, Route, Switch, BrowserRouter } from "react-router-dom";
import { withRouter } from 'react-router'
// import { withStyles } from '@material-ui/core/styles';

import RegisterForm from './RegisterForm'
import LoginForm from './LoginForm'
import DashBoard from "./DashBoard"
import WelocomePage from "./WelocomePage";

import { SignOut } from "./SignOut";
import { PrivateRoute } from "../_helper/myAuth";


// css style
const styles = theme => ({
    formContainer: {
        width: '500px',
        display:"flex",
        justifyContent: 'center',
        alignItems: 'center',
        marginTop: '50px',
        paddingBottom: '50px',
        margin: 'auto',
    }
});

class LandingPage extends Component{

    constructor(props){
        super(props);

        this.state = {
            isRegistered: false,
            value: 0
        };
    }

    render(){
        const {classes} = this.props;        

        return(
            <div>
                <SignOut />
                <div className={classes.formContainer}>
                    <Grid container justify="center">
                        <Switch>
                            <Route exact path="/" component={WelocomePage} />
                            <Route exact path="/register" component={RegisterForm} />
                            <Route exact path="/login" component={LoginForm} />
                            <PrivateRoute path="/dashboard" component={DashBoard}/>
                        </Switch>
                    </Grid>
                </div>
            </div>
        );
    }
}

LandingPage.propTypes = {
    classes: PropTypes.object.isRequired,
}

export default withStyles(styles)(withRouter(LandingPage));

