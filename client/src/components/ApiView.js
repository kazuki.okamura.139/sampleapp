import React, {Component} from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import { Toolbar, Typography, TextField, Button } from '@material-ui/core';

const styles = theme => ({
    result:{
        display: 'block',
        marginTop: theme.spacing.unit * 3,
    }
});

class Result extends Component {
    render(){
        const {classes} = this.props;

        return(
            <div className={classes.result}>
                <Typography variant="body1">{this.props.data.apiMail}</Typography>
                <Typography variant="body1">{this.props.data.apiPass}</Typography>
            </div>
        );
    }
}

Result.propTypes = {
    classes: PropTypes.object.isRequired,
}

export default withStyles(styles)(Result);