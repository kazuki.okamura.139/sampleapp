import React, { Component } from 'react';
import { connect } from 'react-redux';

class UserList extends Component{

    renderList() {
        return this.props.users.map((user) => {
            return (
                <div key={user.name}>
                    <h1>{user.name}</h1>
                    <p>{user.email}</p>
                </div>
            );
        });
    }

    render(){
        return <div>{this.renderList()}</div>
    }
}

const mapStateToProps = (state) => {

    console.log(state);

    return { users: state.users };
}

export default connect(mapStateToProps)(UserList);