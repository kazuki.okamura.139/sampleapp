import React, { Component } from "react";

import PropTypes from 'prop-types';
import AppBar from '@material-ui/core/AppBar';
import { Toolbar, Typography, withStyles } from '@material-ui/core';
import { Apps } from "@material-ui/icons";

const styles = theme => ({
    appBar: {
      position: 'relative',
    },
    icon: {
      marginRight: theme.spacing.unit * 2,
    }
});

class Title extends Component{
    constructor(props){
     super(props);   
    }

    render(){
        const { classes } = this.props;

        return(
            <div>
                <AppBar position="static" className={classes.appBar}>
                    <Toolbar>
                    <Apps className={classes.icon} />
                    <Typography variant="h6" color="inherit" noWrap>
                        研修課題アプリ
                    </Typography>
                    </Toolbar>
                </AppBar>
            </div>
        );
    }
}

Title.propTypes = {
    classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(Title);
