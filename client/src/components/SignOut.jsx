import React, { Component } from "react";
import { Authentication } from "../_helper/authentication";
import { Button } from "@material-ui/core";
import { withRouter } from 'react-router'

export const SignOut = withRouter(({ history }) => (
    Authentication.isAuthenticated ? (
        <Button
            variant="contained" 
            color="primary"
            onClick={() => {
                Authentication.signout(() => history.push('/'))
            }}
        >
            Sign Out
        </Button>
    ) : null
  ))