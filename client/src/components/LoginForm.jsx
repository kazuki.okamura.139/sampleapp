import React, { Component } from "react";

import PropTypes from 'prop-types';
import { FormHelperText, Chip, Grid } from '@material-ui/core';
import { Button, Input, FormControl, InputLabel, InputAdornment, AppBar, Tabs, Tab, Typography } from "@material-ui/core";
import { withRouter } from 'react-router'
import { Link, Redirect } from "react-router-dom";
import { withStyles } from '@material-ui/core/styles';
import { EmailRounded, LockRounded, ExitToApp } from '@material-ui/icons'

import { compose } from 'redux'
import { connect } from 'react-redux';
import Axios from "axios";

import * as actions from '../_actions'
import { Authentication } from "../_helper/authentication";
// import history from '../_helper/history'


// css style
const styles = theme => ({
    heroUnit: {
        backgroundColor: theme.palette.background.paper,
    },
    heroContent: {
        maxWidth: 600,
        margin: '0 auto',
        padding: `${theme.spacing.unit * 8}px 0 ${theme.spacing.unit * 6}px`,
    },
    formContainer: {
        display: 'flex',
        flexWrap: 'wrap',
        flexDirection: 'column',
        padding: '20px',
        maxWidth: 600,
    },
    formControl: {
        paddingTop: theme.spacing.unit,
        paddingBottom: theme.spacing.unit
    },
    textField: {
        marginLeft: theme.spacing.unit,
        marginRight: theme.spacing.unit,
    },
    submitBtn: {
        marginTop: theme.spacing.unit * 3,
    }
});

class LoginForm extends Component{
    constructor(props){
        super(props);

        // this state is only used by this component
        this.state = {
            emailAddress: "",
            passWord: "",
            value: 0,
            redirectToReferrer: false,
            errorMsg: ""
        }

        console.log("This.props is ....");
        console.log(this.props);
    }

    login = () => {
        Authentication.authenticate(() => {
          this.setState(() => ({
            redirectToReferrer: true
          }))
        })
    }

    handleTabChange = (event, value) => {
        this.setState({ value });
    };

    handleChange = (e) => {
        // console.log(e.target.name);

        // Checking Validity of User Input
        const target = e.target;

        // const { setusername } = this.props;

        switch(target.name){

            case "mailaddress":
                console.log("Checking email field");

                this.setState({
                    emailAddress: target.value
                })

                break;

            case "password":
                console.log("Checking password");

                this.setState({
                    passWord: target.value
                })

                break;
            default:
                console.log("No field selected");
                break;
        }
    }

    handleSubmit = () => {
        console.log(this.props);
        console.log(this.state);
        const user = {
            emailAddress: this.state.emailAddress.trim(),
            passWord: this.state.passWord.trim()
        }

        const loginUser = this.props.loginUser(user);

        console.log("--- LOGIN USER ---");
        console.log(loginUser.payload);

        Axios.post('http://localhost:8080/loginuser', loginUser.payload)
            .then( response => {
                console.log("--- API response ---");
                console.log(response);
                console.log(response.data);
                if(response.data == true){
                    this.login();
                }else{
                    Authentication.signout();
                }
            })
            .catch( error => {
                console.log("--- API Error ---");
                console.log(error);
            }

            );
    }

    render(){

        const {classes} = this.props;
        const { redirectToReferrer } = this.state;
        // const { from } = this.props.location.state || { from: { pathname: '/' } };

        if (redirectToReferrer) {
            return <Redirect to="/dashboard" />
        }

        return(
            <div className={classes.heroUnit}>
                <div className={classes.heroContent}>
                        <Typography variant="h2" align="center" color="textSecondary" paragraph>
                            Sign In
                        </Typography>

                    <div className={classes.formContainer}>
                        {this.state.errorMsg != "" ? <Chip label={this.state.errorMsg} className={classes.chip} /> : null}

                        <FormControl className={classes.formControl}>
                            <InputLabel>メールアドレス</InputLabel>
                            <Input id="mailaddress" name="mailaddress"
                                type="email" 
                                className={classes.textField} 
                                onChange={(e) => this.handleChange(e)}
                                startAdornment={
                                    <InputAdornment position="start">
                                        <EmailRounded />
                                    </InputAdornment>
                                } />
                                { this.state.emailError? <FormHelperText>Invalid Emailaddress</FormHelperText>: null }
                        </FormControl>

                        <FormControl className={classes.formControl}>
                            <InputLabel>パスワード</InputLabel>
                            <Input id="password" name="password" 
                                type="password"
                                className={classes.textField} 
                                onChange={(e) => this.handleChange(e)}
                                startAdornment={
                                    <InputAdornment position="start">
                                        <LockRounded />
                                    </InputAdornment>
                                } />
                                { this.state.passwordError? <FormHelperText>Invalid password</FormHelperText>: null }
                        </FormControl>
                            
                        <Button type="submit" 
                            variant="contained" 
                            color="primary" 
                            className={classes.submitBtn}
                            onClick={ () => this.handleSubmit() }>ログイン
                        </Button>
                    </div>

                    <Typography variant="h6" align="center" color="textSecondary" paragraph >
                        Create New Account.
                    </Typography>
                </div>
            </div>
        );
    }
}

// LoginForm.propTypes = {
//     classes: PropTypes.object.isRequired,
// }


// Connecting component to redux
const mapStateToProps = state => {
    return {
        loginUser: state,
    }
}

const mapDispathToProps = dispatch => ({
    loginUser: (user) => dispatch(actions.loginUser(user)),
});

// componentとreduxを接続する。
// export default connect(mapStateToProps, mapDispathToProps)(RegisterForm)

export default compose(withStyles(styles), connect(mapStateToProps, mapDispathToProps))(withRouter(LoginForm));