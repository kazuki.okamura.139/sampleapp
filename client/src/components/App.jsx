import React, {Component} from 'react';
import { withRouter } from 'react-router'
import { Switch, Route } from 'react-router-dom';
// import PropTypes from 'prop-types';
// import AppBar from '@material-ui/core/AppBar';
// import { Toolbar, Typography, TextField, Button } from '@material-ui/core';
// import Result from './Result';
// import ApiView from './ApiView'
// import Axios from 'axios';

import LandingPage from './LandingPage';
import Title from "./Title";

class App extends Component {
    
    constructor(props){
        super(props);
    }

    render(){
        return(
            <div>
                <Title />
                <LandingPage />
            </div>
        );
    }
}

export default withRouter(App);

