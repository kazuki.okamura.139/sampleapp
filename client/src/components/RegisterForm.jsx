import React, { Component } from "react";

import PropTypes from 'prop-types';
import Axios from "axios";
import { FormHelperText, CircularProgress } from '@material-ui/core';
import { Button, Input, FormControl, InputLabel, InputAdornment, AppBar, Tabs, Tab, Chip } from "@material-ui/core";
import { withStyles } from '@material-ui/core/styles';
import { AccountCircle, EmailRounded, LockRounded } from '@material-ui/icons'
import { withRouter } from 'react-router'
import { Link, Redirect } from "react-router-dom";

import { compose } from 'redux'
import { connect } from 'react-redux';

import { Authentication } from "../_helper/authentication";
import * as actions from '../_actions'

import store from '../_helper/store'
// import history from '../_helper/history'


// css style
const styles = theme => ({
    container:{
        backgroundColor: '#f1f1f1',
        margin: 0
    },
    formContainer: {
        display: 'flex',
        flexWrap: 'wrap',
        flexDirection: 'column',
        padding: '20px'
    },
    formControl: {
        paddingTop: theme.spacing.unit,
        paddingBottom: theme.spacing.unit
    },
    textField: {
        marginLeft: theme.spacing.unit,
        marginRight: theme.spacing.unit,
    },
    submitBtn: {
        marginTop: theme.spacing.unit * 3,
    }
});

class RegisterForm extends Component{
    constructor(props){
        super(props);

        // this state is only used by this component
        this.state = {
            isUsernameError: false,
            isEmailError: false,
            isPasswordError: false,
            isRegistering: false,
            username: "",
            email: "",
            password: "",
            value: 0,
            errorMsg: ""
        }

        // console.log("This.props is ....");
        // console.log(this.props);

        // Axios.get('http://localhost:8080/allusers')
        //     .then(re => {
        //         console.log(re);
        //     })
    }

    login = () => {
        Authentication.authenticate(() => {
          this.setState(() => ({
            redirectToReferrer: true
          }))
        })
    }

    handleTabChange = (event, value) => {
        this.setState({ value });
    };

    handleChange = (e) => {
        // console.log(e.target.name);

        // Checking Validity of User Input
        const target = e.target;

        // const { setusername } = this.props;

        switch(target.name){
            case "username":
                console.log("Checking username field");
                if(target.value.match(/^[a-zA-Z0-9]+$/)){
                    // setstate redux
                    console.log("Username is ok");

                    this.setState({
                        usernameError: false,
                        username: target.value
                    })

                    // this.props.setusername(target.value);

                }else{
                    this.setState({
                        usernameError: true,
                    })
                }

                break;

            case "mailaddress":
                console.log("Checking email field");
                // if(target.value.match(/^[a-zA-Z0-9.!#$%&'*+\/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/)){
                //     // set state email to redux

                //     this.setState({
                //         emailError: false
                //     })

                // }else{
                //     this.setState({
                //         emailError: true
                //     })
                // }

                this.setState({
                    email: target.value
                })

                break;

            case "password":
                console.log("Checking password");

                // if(target.value.match(/^(?=.*?[a-z])(?=.*?\d)[a-z\d]{8,100}$/i)){
                //     //setstate to redux

                //     this.setState({
                //         passwordError: false
                //     })
                // }else{
                //     this.setState({
                //         passwordError: true
                //     })
                // }

                this.setState({
                    password: target.value
                })

                break;
            default:
                console.log("No field selected");
                break;
        }
    }

    handleSubmit = () => {
        // console.log(this.props);
        // console.log(this.state);

        this.setState(
            {
                isRegistering: true
            }
        );

        const user = {
            userName: this.state.username,
            emailAddress: this.state.email,
            passWord: this.state.password
        }

        const newUser = this.props.registerUser(user);

        console.log("--- GET REGISTER USER ---");
        console.log(newUser);

        Axios.post('http://localhost:8080/registeruser', newUser.payload)
            .then( response => {
                console.log("--- API response ---");
                console.log(response);

                this.setState({ isRegistering: false });
            })
            .catch( error => {
                console.log("--- API Error ---");
                console.log(error);

                this.setState({ isRegistering: false });
            });

        
        console.log("********Store info********** ");
        console.log(store.getState().registerUser);
        console.log("********Store info********** ");
    }

    render(){

        const {classes} = this.props;
        const { redirectToReferrer } = this.state;
        // const { from } = this.props.location.state || { from: { pathname: '/' } };

        if (redirectToReferrer) {
            return <Redirect to="/dashboard" />
        }

        return(
            <div className={classes.container}>

                <AppBar position="static">
                    <Tabs variant="fullWidth" value={this.state.value} onChange={this.handleTabChange} centered>
                        <Tab label="Register" component={Link} to="/register" />
                        <Tab label="Login" component={Link} to="/login" />
                    </Tabs>
                </AppBar>

                <div className={classes.formContainer}>

                    {this.state.errorMsg != "" ? <Chip label={this.state.errorMsg} className={classes.chip} /> : null}

                    <FormControl className={classes.formControl}>
                        <InputLabel>ユーザー名</InputLabel>
                        <Input id="username" name="username" 
                            className={classes.textField} 
                            onChange={(e) => this.handleChange(e)}
                            startAdornment={
                                <InputAdornment position="start">
                                    <AccountCircle />
                                </InputAdornment>
                            } />
                            { this.state.usernameError? <FormHelperText>Invalid Username</FormHelperText>: null }
                    </FormControl>

                    <FormControl className={classes.formControl}>
                        <InputLabel>メールアドレス</InputLabel>
                        <Input id="mailaddress" name="mailaddress"
                            type="email" 
                            className={classes.textField} 
                            onChange={(e) => this.handleChange(e)}
                            startAdornment={
                                <InputAdornment position="start">
                                    <EmailRounded />
                                </InputAdornment>
                            } />
                            { this.state.emailError? <FormHelperText>Invalid Emailaddress</FormHelperText>: null }
                    </FormControl>

                    <FormControl className={classes.formControl}>
                        <InputLabel>パスワード</InputLabel>
                        <Input id="password" name="password" 
                            type="password"
                            className={classes.textField} 
                            onChange={(e) => this.handleChange(e)}
                            startAdornment={
                                <InputAdornment position="start">
                                    <LockRounded />
                                </InputAdornment>
                            } />
                            { this.state.passwordError? <FormHelperText>Invalid password</FormHelperText>: null }
                    </FormControl>
                        
                    <Button type="submit" 
                        variant="contained" 
                        color="primary" 
                        className={classes.submitBtn}
                        onClick={ () => this.handleSubmit() }>登録
                        { this.state.isRegistering ? <CircularProgress /> : null }
                    </Button> 
                </div>
            </div>
        );
    }
}

RegisterForm.propTypes = {
    classes: PropTypes.object.isRequired,
}

// Connecting component to redux
const mapStateToProps = state => {
    return {
        registerUser: state,
    }
}

const mapDispathToProps = dispatch => ({
    registerUser: (user) => dispatch(actions.registerUser(user)),
});

// componentとreduxを接続する。
// export default connect(mapStateToProps, mapDispathToProps)(RegisterForm)

export default compose(withStyles(styles), connect(mapStateToProps, mapDispathToProps))(withRouter(RegisterForm));