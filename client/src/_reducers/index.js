import { combineReducers } from 'redux';


const userReducer = () => {
    return [
        { name : 'Kazuki', email : 'test@gmail.com' },
        { name : 'Sample', email : 'sample@gmail.com' },
    ];
};

const registerUserReducer = (user = [], action) => {
    if(action.type === 'REGISTER_USER'){
        // add user to array
        // const newUser = [...user, action.payload];

        // console.log("******* Reducer *******");
        // console.log(newUser);
        // console.log("******* Reducer *******");

        return [...user, action.payload];
    }
    return user;
}

const loginUserReducer = (user = [], action) => {
    if(action.type === 'LOGIN_USER'){
        // add user to array
        // const loginUser = [...user, action.payload];
        return [...user, action.payload];
    }

    return user;
}

export default combineReducers({
    users: userReducer,
    registerUser: registerUserReducer,
    loginUser: loginUserReducer
});